from django.db import models
from django.urls import reverse


class Topic(models.Model):
    """Model representing a Topic and Unit for lesson"""
    CLASS_GRADE = (
        ('5', '5. Sınıf'),
        ('6', '6. Sınıf'),
        ('course_1', 'Kur 1'),
        ('course_2', 'Kur 2'),
    )

    grade = models.CharField(
        'Seviye',
        max_length=8,
        choices=CLASS_GRADE,
    )
    unit_name = models.CharField('Ünite adı', max_length=250)
    topic_name = models.CharField('Konu adı', max_length=250, help_text='Konu adını giriniz!')

    def __str__(self):
        """String for representing the Model object."""
        return self.topic_name


class Bty5Topic(models.Model):
    """Model representing a Topic and Unit for 5 grade lessons"""
    UNIT_NAME = (
        ('BİLİŞİM TEKNOLOJİLERİ', 'BİLİŞİM TEKNOLOJİLERİ'),
        ('ETİK VE GÜVENLİK', 'ETİK VE GÜVENLİK'),
        ('İLETİŞİM, ARAŞTIRMA VE İŞ BİRLİĞİ', 'İLETİŞİM, ARAŞTIRMA VE İŞ BİRLİĞİ'),
        ('ÜRÜN OLUŞTURMA', 'ÜRÜN OLUŞTURMA'),
        ('PROBLEM ÇÖZME VE PROGRAMLAMA', 'PROBLEM ÇÖZME VE PROGRAMLAMA'),
    )

    unit_name = models.CharField(
        'Unite Adı',
        max_length=250,
        choices=UNIT_NAME,
    )
    topic_name = models.CharField('Konu adı', max_length=250, help_text='Konu adını giriniz!')

    def __str__(self):
        """String for representing the Model object."""
        return self.topic_name


class Bty6Topic(models.Model):
    """Model representing a Topic and Unit for 5 grade lessons"""
    UNIT_NAME = (
        ('BİLİŞİM TEKNOLOJİLERİ', 'BİLİŞİM TEKNOLOJİLERİ'),
        ('ETİK VE GÜVENLİK', 'ETİK VE GÜVENLİK'),
        ('İLETİŞİM, ARAŞTIRMA VE İŞ BİRLİĞİ', 'İLETİŞİM, ARAŞTIRMA VE İŞ BİRLİĞİ'),
        ('ÜRÜN OLUŞTURMA', 'ÜRÜN OLUŞTURMA'),
        ('PROBLEM ÇÖZME VE PROGRAMLAMA', 'PROBLEM ÇÖZME VE PROGRAMLAMA'),
    )

    unit_name = models.CharField(
        'Unite Adı',
        max_length=250,
        choices=UNIT_NAME,
    )
    topic_name = models.CharField('Konu adı', max_length=250, help_text='Konu adını giriniz!')

    def __str__(self):
        """String for representing the Model object."""
        return self.topic_name


class Course1Topic(models.Model):
    """Model representing a Topic and Unit for 5 grade lessons"""
    UNIT_NAME = (
        ('ETİK, GÜVENLİK VE TOPLUM', 'ETİK, GÜVENLİK VE TOPLUM'),
        ('PROBLEM ÇÖZME VE ALGORİTMALAR', 'PROBLEM ÇÖZME VE ALGORİTMALAR'),
        ('PROGRAMLAMA', 'PROGRAMLAMA'),
    )

    unit_name = models.CharField(
        'Unite Adı',
        max_length=250,
        choices=UNIT_NAME,
    )
    topic_name = models.CharField('Konu adı', max_length=250, help_text='Konu adını giriniz!')

    def __str__(self):
        """String for representing the Model object."""
        return "Kur1_" + self.topic_name


class Course2Topic(models.Model):
    """Model representing a Topic and Unit for 5 grade lessons"""
    UNIT_NAME = (
        ('ROBOT PROGRAMLAMA', 'ROBOT PROGRAMLAMA'),
        ('WEB TABANLI PROGRAMLAMA', 'WEB TABANLI PROGRAMLAMA'),
        ('MOBİL PROGRAMLAMA', 'MOBİL PROGRAMLAMA'),
    )

    unit_name = models.CharField(
        'Unite Adı',
        max_length=250,
        choices=UNIT_NAME,
    )
    topic_name = models.CharField('Konu adı', max_length=250, help_text='Konu adını giriniz!')

    def __str__(self):
        """String for representing the Model object."""
        return "Kur2_" + self.topic_name


class Bty5Question(models.Model):
    """Model representing a Question"""
    text = models.TextField('Soru')
    choice_a = models.CharField('A Şıkkı', max_length=300)
    choice_b = models.CharField('B Şıkkı', max_length=300)
    choice_c = models.CharField('C Şıkkı', max_length=300)
    choice_d = models.CharField('D Şıkkı', max_length=300)

    RIGHT_CHOICE = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
    )

    right_choice = models.CharField(
        'Doğru Cevap',
        max_length=1,
        choices=RIGHT_CHOICE,
    )
    # ManyToManyField used because Question can have many topic. Topics can cover many questions.
    topic = models.ForeignKey(Bty5Topic, on_delete=models.SET_NULL, null=True, help_text='Sorunun ait olduğu konuları seçin')
    date_modified = models.DateField(auto_now=True)

    def get_absolute_url(self):
        """Returns the url to access a particular question instance."""
        return reverse('question-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return self.text


class Bty6Question(models.Model):
    """Model representing a Question"""
    text = models.TextField('Soru')
    choice_a = models.CharField('A Şıkkı', max_length=300)
    choice_b = models.CharField('B Şıkkı', max_length=300)
    choice_c = models.CharField('C Şıkkı', max_length=300)
    choice_d = models.CharField('D Şıkkı', max_length=300)

    RIGHT_CHOICE = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
    )

    right_choice = models.CharField(
        'Doğru Cevap',
        max_length=1,
        choices=RIGHT_CHOICE,
    )
    # ManyToManyField used because Question can have many topic. Topics can cover many questions.
    topic = models.ForeignKey(Bty6Topic, on_delete=models.SET_NULL, null=True, help_text='Sorunun ait olduğu konuları seçin')
    date_modified = models.DateField(auto_now=True)

    def get_absolute_url(self):
        """Returns the url to access a particular question instance."""
        return reverse('question-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return self.text


class Course1Question(models.Model):
    """Model representing a Question"""
    text = models.TextField('Soru')
    choice_a = models.CharField('A Şıkkı', max_length=300)
    choice_b = models.CharField('B Şıkkı', max_length=300)
    choice_c = models.CharField('C Şıkkı', max_length=300)
    choice_d = models.CharField('D Şıkkı', max_length=300)
    choice_e = models.CharField('E Şıkkı', max_length=300)

    RIGHT_CHOICE = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('E', 'E'),
    )

    right_choice = models.CharField(
        'Doğru Cevap',
        max_length=1,
        choices=RIGHT_CHOICE,
    )
    # ManyToManyField used because Question can have many topic. Topics can cover many questions.
    topic = models.ForeignKey(Course1Topic, on_delete=models.SET_NULL, null=True, help_text='Sorunun ait olduğu konuları seçin')
    date_modified = models.DateField(auto_now=True)

    def get_absolute_url(self):
        """Returns the url to access a particular question instance."""
        return reverse('question-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return self.text


class Course2Question(models.Model):
    """Model representing a Question"""
    text = models.TextField('Soru')
    choice_a = models.CharField('A Şıkkı', max_length=300)
    choice_b = models.CharField('B Şıkkı', max_length=300)
    choice_c = models.CharField('C Şıkkı', max_length=300)
    choice_d = models.CharField('D Şıkkı', max_length=300)
    choice_e = models.CharField('E Şıkkı', max_length=300)

    RIGHT_CHOICE = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('E', 'E'),
    )

    right_choice = models.CharField(
        'Doğru Cevap',
        max_length=1,
        choices=RIGHT_CHOICE,
    )
    # ManyToManyField used because Question can have many topic. Topics can cover many questions.
    topic = models.ForeignKey(Course2Topic, on_delete=models.SET_NULL, null=True, help_text='Sorunun ait olduğu konuları seçin')
    date_modified = models.DateField(auto_now=True)

    def get_absolute_url(self):
        """Returns the url to access a particular question instance."""
        return reverse('question-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return self.text


class Question(models.Model):
    """Model representing a Question"""
    text = models.TextField('Soru')
    choice_a = models.CharField('A Şıkkı', max_length=300)
    choice_b = models.CharField('B Şıkkı', max_length=300)
    choice_c = models.CharField('C Şıkkı', max_length=300)
    choice_d = models.CharField('D Şıkkı', max_length=300)

    RIGHT_CHOICE = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
    )

    right_choice = models.CharField(
        'Doğru Cevap',
        max_length=1,
        choices=RIGHT_CHOICE,
    )
    # ManyToManyField used because Question can have many topic. Topics can cover many questions.
    topic = models.ForeignKey(Topic, on_delete=models.SET_NULL, null=True, help_text='Sorunun ait olduğu konuları seçin')
    date_modified = models.DateField(auto_now=True)

    def get_absolute_url(self):
        """Returns the url to access a particular question instance."""
        return reverse('question-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return self.text
