from django.contrib import admin
from .models import Topic, Question
from .models import Bty5Topic, Bty6Topic, Course1Topic, Course2Topic
from .models import Bty5Question, Bty6Question, Course1Question, Course2Question
# Register your models here.


class TopicAdmin(admin.ModelAdmin):
    list_display = ('grade', 'unit_name', 'topic_name')


class Bty5TopicAdmin(admin.ModelAdmin):
    list_display = ('unit_name', 'topic_name')


class Bty6TopicAdmin(admin.ModelAdmin):
    list_display = ('unit_name', 'topic_name')


class Course1TopicAdmin(admin.ModelAdmin):
    list_display = ('unit_name', 'topic_name')


class Course2TopicAdmin(admin.ModelAdmin):
    list_display = ('unit_name', 'topic_name')


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('text', 'choice_a', 'choice_b', 'choice_c', 'choice_d', 'right_choice', 'topic')
    list_filter = ('topic', 'date_modified')
    search_fields = ['text', 'choice_a']


class Bty5QuestionAdmin(admin.ModelAdmin):
    list_display = ('text', 'choice_a', 'choice_b', 'choice_c', 'choice_d', 'right_choice', 'topic')
    list_filter = ('topic', 'date_modified')
    search_fields = ['text', 'choice_a']


class Bty6QuestionAdmin(admin.ModelAdmin):
    list_display = ('text', 'choice_a', 'choice_b', 'choice_c', 'choice_d', 'right_choice', 'topic')
    list_filter = ('topic', 'date_modified')
    search_fields = ['text', 'choice_a']


class Course1QuestionAdmin(admin.ModelAdmin):
    list_display = ('text', 'choice_a', 'choice_b', 'choice_c', 'choice_d', 'choice_e', 'right_choice', 'topic')
    list_filter = ('topic', 'date_modified')
    search_fields = ['text', 'choice_a']


class Course2QuestionAdmin(admin.ModelAdmin):
    list_display = ('text', 'choice_a', 'choice_b', 'choice_c', 'choice_d', 'choice_e', 'right_choice', 'topic')
    list_filter = ('topic', 'date_modified')
    search_fields = ['text', 'choice_a']


# Register the Topic and Question class with the associated model
admin.site.register(Topic, TopicAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Bty5Topic, Bty5TopicAdmin)
admin.site.register(Bty6Topic, Bty6TopicAdmin)
admin.site.register(Course1Topic, Course1TopicAdmin)
admin.site.register(Course2Topic, Course2TopicAdmin)
admin.site.register(Bty5Question, Bty5QuestionAdmin)
admin.site.register(Bty6Question, Bty6QuestionAdmin)
admin.site.register(Course1Question, Course1QuestionAdmin)
admin.site.register(Course2Question, Course2QuestionAdmin)

