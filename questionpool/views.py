from django.shortcuts import render
from django.http import HttpResponse
from .models import Topic, Question, Bty5Question, Bty6Question, Course1Question, Course2Question
from .models import Bty5Topic
from django.views import generic
from docx import Document
from docx.shared import Inches
from docx.enum.text import WD_ALIGN_PARAGRAPH


def index(request):
    """View function for home page of site."""

    # just test
    bty5questions = Bty5Question.objects.all().count()
    bty6questions = Bty6Question.objects.all().count()
    course1questions = Course1Question.objects.all().count()
    course2questions = Course2Question.objects.all().count()
    total_questions = bty5questions + bty6questions + course1questions + course2questions

    context = {
        'bty5questions': bty5questions,
        'bty6questions': bty6questions,
        'course1questions': course1questions,
        'course2questions': course2questions,
        'total_questions': total_questions,
    }

    # Render the HTML index.html with context data
    return render(request, 'index.html', context=context)


def go_class(request, class_name):
    if class_name == 'Bty5':
        questions = Bty5Question.objects.all()
    elif class_name == 'Bty6':
        questions = Bty6Question.objects.all()
    elif class_name == 'Course1':
        questions = Course1Question.objects.all()
    elif class_name == 'Course2':
        questions = Course2Question.objects.all()
    else:
        # There is no class with this name error will return
        questions = Bty5Question.objects.all()

    total_questions = questions.count()

    # Available questions for list of units
    num_unit1_topic1 = questions.filter(topic__topic_name__exact='Bilişim Teknolojilerinin Günlük Yaşamdaki Önemi').count()
    num_unit1_topic2 = questions.filter(topic__topic_name__exact='Bilgisayar Sistemleri').count()
    num_unit1_topic3 = questions.filter(topic__topic_name__exact='Dosya Yönetimi').count()
    num_unit1 = num_unit1_topic1 + num_unit1_topic2 + num_unit1_topic3

    num_unit2_topic1 = questions.filter(topic__topic_name__exact='Etik Değerler').count()
    num_unit2_topic2 = questions.filter(topic__topic_name__exact='Dijital Vatandaşlık').count()
    num_unit2_topic3 = questions.filter(topic__topic_name__exact='Gizlilik ve Güvenlik').count()
    num_unit2 = num_unit2_topic1 + num_unit2_topic2 + num_unit2_topic3

    num_unit3_topic1 = questions.filter(topic__topic_name__exact='Bilgisayar Ağları').count()
    num_unit3_topic2 = questions.filter(topic__topic_name__exact='Araştırma').count()
    num_unit3_topic3 = questions.filter(topic__topic_name__exact='İletişim Teknolojileri ve İş Birliği').count()
    num_unit3 = num_unit3_topic1 + num_unit3_topic2 + num_unit3_topic3

    num_unit4_topic1 = questions.filter(topic__topic_name__exact='Görsel İşleme Programları').count()
    num_unit4_topic2 = questions.filter(topic__topic_name__exact='Kelime İşlemci Programları').count()
    num_unit4_topic3 = questions.filter(topic__topic_name__exact='Sunu Programları').count()
    num_unit4 = num_unit4_topic1 + num_unit4_topic2 + num_unit4_topic3

    num_unit5_topic1 = questions.filter(topic__topic_name__exact='Problem Çözme Kavramları ve Yaklaşımları').count()
    num_unit5_topic2 = questions.filter(topic__topic_name__exact='Programlama').count()
    num_unit5 = num_unit5_topic1 + num_unit5_topic2

    context = {
        'total_questions': total_questions,
        'num_unit1_topic1': num_unit1_topic1,
        'num_unit1_topic2': num_unit1_topic2,
        'num_unit1_topic3': num_unit1_topic3,
        'num_unit2_topic1': num_unit2_topic1,
        'num_unit2_topic2': num_unit2_topic2,
        'num_unit2_topic3': num_unit2_topic3,
        'num_unit3_topic1': num_unit3_topic1,
        'num_unit3_topic2': num_unit3_topic2,
        'num_unit3_topic3': num_unit3_topic3,
        'num_unit4_topic1': num_unit4_topic1,
        'num_unit4_topic2': num_unit4_topic2,
        'num_unit4_topic3': num_unit4_topic3,
        'num_unit5_topic1': num_unit5_topic1,
        'num_unit5_topic2': num_unit5_topic2,
        'num_unit1': num_unit1,
        'num_unit2': num_unit2,
        'num_unit3': num_unit3,
        'num_unit4': num_unit4,
        'num_unit5': num_unit5,
        'class_name': class_name,
    }

    return render(request, 'go_class.html', context=context)


class QuestionListView(generic.ListView):
    model = Question


class Bty5QuestionListView(generic.ListView):
    model = Bty5Question


class QuestionDetailView(generic.DetailView):
    model = Bty5Question
    template_name = 'questionpool/question_detail.html'


class QuestionTopicListView(generic.ListView):
    model = Bty5Question
    template_name = 'questionpool/question_topic_list.html'

    def get_queryset(self):
        return Bty5Question.objects.filter(topic__topic_name__exact=self.kwargs['topic_name'])

    # topic_name added to context
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['topic_name'] = self.kwargs['topic_name']
        context['class_name'] = self.kwargs['class_name']

        num_visits = self.request.session.get('num_visits', 0)
        self.request.session['num_visits'] = num_visits + 1

        my_question_list = self.request.session.get('my_question_list', [])
        self.request.session['my_question_list'] = my_question_list

        if self.request.GET.getlist('selected_question'):
            self.request.session['my_question_list'] += self.request.GET.getlist('selected_question')
            self.request.session['my_question_list'] = list(set(self.request.session['my_question_list']))
        else:
            self.request.session['my_question_list'] = self.request.session['my_question_list']

        context['num_visits'] = num_visits
        context['selected_question'] = self.request.GET.getlist('selected_question')
        context['my_question_list'] = self.request.session['my_question_list']

        return context


# Doos docx for single question
def do_doc(request, question_id):
    # Just take one question with id
    question = Bty5Question.objects.get(pk=question_id)

    document = Document()

    content = {"heading": f"# {question.pk} Numaralı Soru #",
               "description": question.text,
               }

    document.add_heading(content["heading"], 0)

    document.add_paragraph(content["description"])

    document.add_heading('Cevap Şıkları', level=1)

    document.add_paragraph(f'A) {question.choice_a}', style='List')
    document.add_paragraph(f'B) {question.choice_b}', style='List')
    document.add_paragraph(f'C) {question.choice_c}', style='List')
    document.add_paragraph(f'D) {question.choice_d}', style='List')

    document.add_heading('Doğru Şık', level=2)

    document.add_paragraph(f'Cevap: {question.right_choice}', style='ListBullet')
    document.add_heading('Konu', level=2)
    document.add_paragraph(f'{question.topic}', style='ListBullet')
    document.add_heading('Düzenleme Tarihi', level=2)
    document.add_paragraph(f'{question.date_modified}', style='ListBullet')

    # For help: https://stackoverflow.com/questions/48396953/how-to-create-a-docx-file-using-django-framework
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    response['Content-Disposition'] = f'attachment; filename="{question.pk}.docx"'
    document.save(response)
    return response


# Does question list with chosen topics
def do_list(request, question_topic, with_answer):
    # All question word list
    if question_topic == "all":
        questions = Bty5Question.objects.all()
    else:
        questions = Bty5Question.objects.filter(topic__topic_name__exact=question_topic)

    document = Document()
    paragraph = document.add_paragraph("""2018-2019 Eğitim Öğretim Yılı Kadımehmet Ortaokulu
    Bilişim Teknolojileri Dersi _.Dönem _.Yazılı Sınavı""", style='Subtitle')
    # https://python-docx.readthedocs.io/en/latest/api/enum/WdAlignParagraph.html#wdparagraphalignment
    paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
    document.add_paragraph('ADI:', style='List')
    document.add_paragraph('SOYADI:', style='List')
    document.add_paragraph('SINIFI:', style='List')
    document.add_paragraph('NUMARASI:', style='List')

    for counter, question in enumerate(questions, start=1):

        document.add_paragraph(f'{counter}. {question.text}')

        document.add_paragraph(f'A) {question.choice_a}', style='List')
        document.add_paragraph(f'B) {question.choice_b}', style='List')
        document.add_paragraph(f'C) {question.choice_c}', style='List')
        document.add_paragraph(f'D) {question.choice_d}', style='List')
        document.add_paragraph()

        # docx document wiht answer key or not
        if with_answer == 'yes':
            document.add_paragraph(f'Cevap: {question.right_choice}')
        else:
            # do nothing :)
            pass

    # For help: https://stackoverflow.com/questions/48396953/how-to-create-a-docx-file-using-django-framework
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    response['Content-Disposition'] = f'attachment; filename="all_questions.docx"'
    document.save(response)
    return response


# Does question list with chosen questions in basket
def do_basket_list(request, question_topic, with_answer):
    # pk list for chosen questions
    pk_list = request.session['my_question_list']

    # All question word list
    if question_topic == "all":
        questions = Bty5Question.objects.all()
    else:
        questions = Bty5Question.objects.filter(pk__in=pk_list)

    document = Document()
    paragraph = document.add_paragraph("""2018-2019 Eğitim Öğretim Yılı Kadımehmet Ortaokulu
    Bilişim Teknolojileri Dersi _.Dönem _.Yazılı Sınavı""", style='Subtitle')
    # https://python-docx.readthedocs.io/en/latest/api/enum/WdAlignParagraph.html#wdparagraphalignment
    paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
    document.add_paragraph('ADI:', style='List')
    document.add_paragraph('SOYADI:', style='List')
    document.add_paragraph('SINIFI:', style='List')
    document.add_paragraph('NUMARASI:', style='List')

    for counter, question in enumerate(questions, start=1):

        document.add_paragraph(f'{counter}. {question.text}')

        document.add_paragraph(f'A) {question.choice_a}', style='List')
        document.add_paragraph(f'B) {question.choice_b}', style='List')
        document.add_paragraph(f'C) {question.choice_c}', style='List')
        document.add_paragraph(f'D) {question.choice_d}', style='List')
        document.add_paragraph()

        # docx document wiht answer key or not
        if with_answer == 'yes':
            document.add_paragraph(f'Cevap: {question.right_choice}')
        else:
            # do nothing :)
            pass

    # For help: https://stackoverflow.com/questions/48396953/how-to-create-a-docx-file-using-django-framework
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    response['Content-Disposition'] = f'attachment; filename="all_questions.docx"'
    document.save(response)
    return response