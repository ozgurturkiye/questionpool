from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<class_name>/<int:pk>', views.QuestionDetailView.as_view(), name='question-detail'),
    path('report/<int:question_id>/report', views.do_doc, name='do-doc'),
    path('<class_name>', views.go_class, name='go-class'),
    path('<class_name>/<topic_name>', views.QuestionTopicListView.as_view(), name='class-topic'),
    path('all-questions/<question_topic>/<with_answer>/do-list', views.do_list, name='do-list'),
    path('all-questions/<question_topic>/<with_answer>/do-basket-list', views.do_basket_list, name='do_basket_list'),
]